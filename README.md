dahid2prometheus - a prometheus exporter for DAHDI
==================================================

This is a small project in the `go` programming language, obtaining DAHDI
statistic via the DAHDI_SPANSTAT ioctl() and exposing the data in
prometheus exporter syntax over HTTP for prometheus to collect/scrape
them.

It's almost my first go project, and I had the basics put together in
about 30mins.  Your mileage may vary!

Usage
-----

```
Usage of dahdi2prometheus:
  -http string
        local HTTP listen host:port (default ":2113")
```

So you simply run the dahdi2prometheus somewhere and

* tell it on which HTTP port to listen for prometheus to scrape
* configure prometheus to actually scrape with a section as stated below

```
scrape_configs:
  - job_name: dahdi
    static_configs:
      - targets: ['hostname:2113']
```

Grafana
-------

There's a [sample grafana dashboard](grafana-dashboard/dahdi.json)

![sample grafana dashboard](grafana-dashboard/grafana-dahdi-dashboard.png?raw=true "grafana dashboard for DAHDI")

Credits
-------

All the real workhorse code behind this project is in the following
upstream libraries I'm using:

* [prometheus/client_golang](https://github.com/prometheus/client_golang)

This project is just gluing together the above two libraries.

License
-------

I'm usually much more in favor of copyleft licenses, but given the two
libraries I use are MIT and Apache 2.0, I decided to go for a permissive
license in this project, too.  So the code is released under Apache 2.0,
see the [COPYING](COPYING) file for details.
